import React from 'react';
import { Card, CardTitle } from 'material-ui/Card';


const HomePage = () => (
  <Card className="container">
    <CardTitle title="Aimbet Pronostics E-sportifs" subtitle="Bienvenue sur le site d'Aimbet, site de pronostics en live de matchs E-sportifs.Si vous avez un compte merci de vous connecter, sinon, merci de créer un compte." />
  </Card>
);

export default HomePage;
