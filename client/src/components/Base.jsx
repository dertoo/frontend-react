import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';


const Base = ({ children }) => (
  <div>
    <div className="top-bar">
      <div className="top-bar-left">
        <IndexLink to="/">Aimbet</IndexLink>
      </div>

      <div className="top-bar-right">
        <Link to="/login">Connexion</Link>
        <Link to="/signup">S'enregistrer</Link>
      </div>

    </div>

    {children}

  </div>
);

Base.propTypes = {
  children: PropTypes.object.isRequired
};

export default Base;
